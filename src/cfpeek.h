/* This file is part of cfpeek
   Copyright (C) 2011-2021 Sergey Poznyakoff

   Cfpeek is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Cfpeek is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with cfpeek.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sysexits.h>
#include "grecs.h"
#include "wordsplit.h"

#ifndef EX_OK
# define EX_OK        0
#endif
#define EX_NOTFOUND   1
#define EX_PARSE      2
#define EX_SCRIPTFAIL 3

extern char *program_name;
extern char *script_file;
extern char *script_expr;
extern char *script_init_expr;
extern char *script_done_expr;

void script_select(const char *lang);
void script_init(void);
int script_run(struct grecs_node *node);
void script_done(void);

void guile_init(void);
void guile_apply(struct grecs_node *node);
void guile_done(void);




