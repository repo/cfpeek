# This file is part of cfpeek
# Copyright (C) 2011-2021 Sergey Poznyakoff
#
# Cfpeek is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Cfpeek is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cfpeek.  If not, see <http://www.gnu.org/licenses/>.

include Makefile

BUILT_SOURCES += $(LISTINGS) scripting.texi

ifneq ($(CFPEEK_GUILE_ENABLED),1)
  $(error Please, enable support for Guile when building from Git sources)
endif

scripting.texi: $(top_srcdir)/src/guile-procedures.texi
	cat $(top_srcdir)/src/guile-procedures.texi | \
	 tr '\f' '\\' | sed '/^\\/d' > scripting.texi

$(LISTINGS): cfpeek.texi

%.texi: %.scm
	$(AM_V_GEN)sed '/^;;;;/d' $< > $@ && \
	emacs -batch -l $(top_srcdir)/$(IMPRIMATUR_MODULE_DIR)/untabify.el $@

%.lst: cfpeek.texi
	$(AM_V_GEN)cmd=`sed -n 's/@runex{$* *, *\(.*\)}/\1/p' cfpeek.texi`; \
	if test -z "$$cmd"; then \
	  echo >&2 "no command to build $@"; \
          exit 1; \
        fi; \
        cmdline=`echo "$$cmd" | sed 's/\\\\,/,/g'`; \
        PATH=$(top_builddir)/src:$$PATH eval $$cmdline > $@-t 2>$@-e; EC=$$?; \
        if test -s $@-e; then \
          cat $@-e >&2; \
          rm $@-e; \
          exit 1; \
	fi; \
	rm $@-e; \
        if test $$EC -ne 0; then \
          echo >&2 "\"$$cmdline\" exited with status $$EC"; \
          exit 1; \
        fi; \
	if test -s $@-t; then \
	  emacs -batch -l $(top_srcdir)/$(IMPRIMATUR_MODULE_DIR)/untabify.el $@-t; \
          mv $@-t $@; \
        else \
          echo >&2 "$@: command produced empty result"; \
          exit 1;\
        fi


